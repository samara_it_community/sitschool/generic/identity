#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER hydra WITH password 'secret';
    CREATE DATABASE hydra;
    GRANT ALL PRIVILEGES ON DATABASE hydra TO hydra;
    CREATE USER kratos WITH password 'secret';
    CREATE DATABASE kratos;
    GRANT ALL PRIVILEGES ON DATABASE kratos TO kratos;
EOSQL
